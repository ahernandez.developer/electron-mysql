const { createWindow } = require("./main");
const { app } = require("electron");
const path = require('path');
require('./database');

require('electron-reload')(process.cwd(), {
    electron: path.join(process.cwd(), 'node_modules', '.bin', 'electron'),
    hardResetMethod: 'exit'
});

app.allowRendererProcessReuse = false;
app.whenReady().then(createWindow);
