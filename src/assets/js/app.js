const createProductForm = document.querySelector("#createProductForm");

const { remote } = require("electron");
const main = remote.require("./main");



createProductForm.addEventListener("submit", async (e) => {
  e.preventDefault();
  const productName = document.querySelector("#name").value;
  const productPrice = document.querySelector("#price").value;
  const productDescription = document.querySelector("#description").value;
  const newProduct = {
    name: productName,
    price: productPrice,
    description: productDescription,
  };
  const result = await main.createProduct(newProduct);
  
  fetchProducts();
});

function renderProducts(products) {
    const html_products = document.querySelector('#products');
    console.log(products);
    console.log("entro");
    html_products.innerHTML = "";
    products.forEach((p) => {
    const template = /*html*/ `
            <div class="card col-4 p-2">
                <div class="card-body">
                    <div class="card-header">
                   ${p.id} - ${p.name}
                    </div>
                    ${p.description}
                    <br/>
                    $ ${p.price}
                </div>
                <div class="card-footer">
                    <button class="btn btn-danger btn-block" onclick="deleteProduct(${p.id})">DELETE</button>
                    <br/>
                    <button class="btn btn-info btn-block" onclick="editProduct(${p.id})">EDIT</button>
                </div>
            </div>        
        `;

        html_products.innerHTML +=template; 
  });
}

async function fetchProducts() {
  let result = await main.fetchProducts();
  renderProducts(result);
}

async function deleteProduct(id)
{
    let response = confirm('are you sure of delete the product');
    if(response)
    {
        const result = await main.deleteProduct(id);
        console.log(result);
        fetchProducts();
    }
    
}


async function editProduct(id)
{
    main.createEditWindow(0);
    
}


fetchProducts();