const { BrowserWindow } = require("electron");
const { getConnection } = require("./database");
let window;
let editWindow;
function createWindow() {
  window = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      enableRemoteModule: true,
    },
  });

  window.loadFile("src/views/index.html");
}

function createEditWindow() {
  editWindow = new BrowserWindow({
    width: 400,
    height: 300,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      enableRemoteModule: true,
    },
  });


  editWindow.loadFile("src/views/edit.html");

}

async function createProduct(product) {
  const conn = await getConnection();

  product.price = parseFloat(product.price);
  const result = await conn.query("INSERT INTO products SET ?", product);
  product.id = result.insertId;
  return product;
}

async function fetchProducts() {
  const conn = await getConnection();
  const results = await conn.query("SELECT * FROM products");
  return results;
}

async function deleteProduct(id) {
  const conn = await getConnection();
  const result = await conn.query("DELETE FROM products where id=" + id);
  return result;
}

module.exports = {
  createWindow,
  createProduct,
  fetchProducts,
  deleteProduct,
  createEditWindow
};
